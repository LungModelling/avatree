# CMake generated Testfile for 
# Source directory: E:/Libraries/eigen-3.2.7
# Build directory: E:/Libraries/eigen-3.2.7/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("Eigen")
subdirs("doc")
subdirs("test")
subdirs("blas")
subdirs("lapack")
subdirs("unsupported")
subdirs("demos")
subdirs("scripts")

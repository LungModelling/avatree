# Install script for directory: E:/Libraries/eigen-3.2.7/Eigen/src/Core

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "E:/Libraries/Install/Eigen")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/Eigen/src/Core" TYPE FILE FILES
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Array.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/ArrayBase.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/ArrayWrapper.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Assign.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Assign_MKL.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/BandMatrix.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Block.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/BooleanRedux.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/CommaInitializer.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/CoreIterators.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/CwiseBinaryOp.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/CwiseNullaryOp.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/CwiseUnaryOp.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/CwiseUnaryView.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/DenseBase.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/DenseCoeffsBase.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/DenseStorage.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Diagonal.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/DiagonalMatrix.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/DiagonalProduct.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Dot.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/EigenBase.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Flagged.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/ForceAlignedAccess.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Functors.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Fuzzy.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/GeneralProduct.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/GenericPacketMath.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/GlobalFunctions.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/IO.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Map.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/MapBase.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/MathFunctions.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Matrix.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/MatrixBase.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/NestByValue.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/NoAlias.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/NumTraits.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/PermutationMatrix.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/PlainObjectBase.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/ProductBase.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Random.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Redux.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Ref.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Replicate.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/ReturnByValue.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Reverse.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Select.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/SelfAdjointView.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/SelfCwiseBinaryOp.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/SolveTriangular.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/StableNorm.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Stride.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Swap.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Transpose.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Transpositions.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/TriangularMatrix.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/VectorBlock.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/VectorwiseOp.h"
    "E:/Libraries/eigen-3.2.7/Eigen/src/Core/Visitor.h"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("E:/Libraries/eigen-3.2.7/build/Eigen/src/Core/products/cmake_install.cmake")
  include("E:/Libraries/eigen-3.2.7/build/Eigen/src/Core/util/cmake_install.cmake")
  include("E:/Libraries/eigen-3.2.7/build/Eigen/src/Core/arch/cmake_install.cmake")

endif()


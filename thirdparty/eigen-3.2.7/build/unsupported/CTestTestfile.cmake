# CMake generated Testfile for 
# Source directory: E:/Libraries/eigen-3.2.7/unsupported
# Build directory: E:/Libraries/eigen-3.2.7/build/unsupported
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("Eigen")
subdirs("doc")
subdirs("test")
